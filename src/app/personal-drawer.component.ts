import { Component } from '@angular/core';

@Component({
  selector: 'personal-drawer',
  template: `
  	<kyc-loader [title]="title"></kyc-loader>
  `
 })
export class PersonalDrawerComponent {
  title = 'personal';
}
