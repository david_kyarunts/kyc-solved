import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { KycPersonalComponent } from './kyc-personal/kyc-personal.component';

@NgModule({
  declarations: [
    KycPersonalComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  exports: [
    KycPersonalComponent
  ],
  entryComponents: [
    KycPersonalComponent
    
  ]
})
export class KycAppsModule { 
}
