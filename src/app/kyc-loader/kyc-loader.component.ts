import { Component, OnInit, Input, ViewContainerRef, ViewChild, ComponentFactoryResolver, ComponentFactory } from '@angular/core';
import { KycPersonalComponent } from '../kyc-apps/kyc-personal/kyc-personal.component'


@Component({
	selector: 'kyc-loader',
	template: `
	<ng-template #personal>
		This is personal
	</ng-template>
	`
})
export class KycLoaderComponent implements OnInit {

	@ViewChild('personal', {read: ViewContainerRef}) personalContainer;
	@Input() title;

	personalComponentFactory: ComponentFactory<any> = this.componentResolver.resolveComponentFactory(KycPersonalComponent); 

	constructor(
		private componentResolver: ComponentFactoryResolver
	) {}
	

	ngOnInit() {
		if (this.title === "personal") {
			this.personalContainer.createComponent(this.personalComponentFactory);
		}
	}
}