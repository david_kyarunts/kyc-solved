import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';

import { PersonalDrawerComponent } from './personal-drawer.component';
import { EmploymentDrawerComponent } from './employment-drawer.component';
import { KycLoaderComponent } from './kyc-loader/kyc-loader.component';
import { KycAppsModule } from './kyc-apps/kyc-apps.module';

@NgModule({
  declarations: [
    PersonalDrawerComponent,
    EmploymentDrawerComponent,
    KycLoaderComponent
  ],
  imports: [
    BrowserModule,
    KycAppsModule
  ],
  providers: [],
  entryComponents: [
  	PersonalDrawerComponent,
  	EmploymentDrawerComponent
  ]
})
export class AppModule { 

	ngDoBootstrap(appRef: ApplicationRef) {
		try {
			appRef.bootstrap(PersonalDrawerComponent);
			appRef.bootstrap(EmploymentDrawerComponent);
		}
		catch(error) {
			appRef.bootstrap(EmploymentDrawerComponent);			
		}
	}
}
