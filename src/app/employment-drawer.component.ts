import { Component } from '@angular/core';

@Component({
  selector: 'employment-drawer',
  template: `
  	<kyc-loader [title]="title"></kyc-loader>
  `
})
export class EmploymentDrawerComponent {
  title = 'personal';
}
