import { IcandoPage } from './app.po';

describe('icando App', () => {
  let page: IcandoPage;

  beforeEach(() => {
    page = new IcandoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
